import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.Test;

public class TestBaseEdge {

    @Test
    public void loginWebex(){
        System.setProperty("webdriver.edge.driver", "C:\\Gitea\\WMSTI_Test_Automation\\src\\main\\resources\\Msedgedriver\\msedgedriver.exe");
        WebDriver driverEdge = new EdgeDriver();
        driverEdge.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        driverEdge.quit();
    }
}
