import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class BaseTestChrome extends BaseTest {

//    @FindBy(name = "userId")
//    WebElement emailOrCiamIDField;
//
//    @FindBy(id = "password")
//    WebElement ciamPassoword;



    @Test
    public void myPortalLoginNegativeTest(){
        WebElement emailOrCiamIDField = driver.findElement(By.name("userId"));
        emailOrCiamIDField.sendKeys("Marina.Tkachenko@telekom.com");
        WebElement ciamPassoword = driver.findElement(By.id("password"));
        ciamPassoword.sendKeys("WrongPass");
        WebElement logInButton = driver.findElement(By.xpath("//*[@id=\"submit\"]"));
        logInButton.click();

        WebElement errorMessage = driver.findElement(By.xpath("//*[@id=\"_loginForm_\"]/div[3]"));
        Assert.assertTrue(errorMessage.isDisplayed(),"'Need help signing' text is displayed");

    }

    @Test
    public void myPortalLoginPositiveTest(){
        WebElement checkTSSoLoginText = driver.findElement(By.cssSelector("#headline-container > h1"));
        Assert.assertTrue(checkTSSoLoginText.isDisplayed(),"T-SSO Login text is displayed");

        WebElement forgottenYourPasswordLink = driver.findElement(By.linkText("Forgotten your password?"));
        Assert.assertTrue(forgottenYourPasswordLink.isDisplayed(),"Forgotten your password is displayed");

        WebElement emailOrCiamIDField = driver.findElement(By.name("userId"));
        emailOrCiamIDField.sendKeys("Marina.Tkachenko@telekom.com");

        WebElement ciamPassoword = driver.findElement(By.id("password"));
        ciamPassoword.sendKeys("UnterDenLinden10");
        driver.findElement(By.xpath("//*[@id=\"submit\"]")).click();

        Assert.assertEquals(driver.getCurrentUrl(),"https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag&from_login=true","Url is correct");
        WebElement startSeiteElement = driver.findElement(By.xpath("//*[@id=\"dashboard_nav_home_item\"]"));
        Assert.assertTrue(startSeiteElement.isDisplayed(),"Start Seite Element is displayed");

        WebElement startSeiteIcon = driver.findElement(By.className("icon-ng-home"));
        Assert.assertTrue(startSeiteIcon.isDisplayed(),"Start Seite Icon is displayed");

    }
}
