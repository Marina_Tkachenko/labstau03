package Selenide;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.*;

public class Logintest {
    String url = "https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag";
    @Test
    public void signInWebexSelenide() {
        System.setProperty("webdriver.chrome.driver", "C:\\Gitea\\WMSTI_Test_Automation\\src\\main\\resources\\ChromeDriver\\chromedriver92.exe");
        open(url);
        $(By.xpath("//button[@id='guest_signin_split_button-action']")).shouldBe(Condition.visible).click();
        $(By.id("IDToken1")).val("Marina.Tkachenko@telekom.com");
        $(By.name("btnOK")).click();

        $(By.id("openingMessage")).shouldBe(Condition.visible);
    }
    public void myPortalLoginNegativeTest(){

        $(By.name("btnOK")).shouldBe(Condition.enabled).click();
        $(By.name("userId")).val("Marina.Tkachenko@telekom.com");
        $(By.id("password")).val("XXXXX");
        $(By.xpath("//*[@id=\"submit\"]")).click();

        $$(By.xpath("//li[contains(@class,'m_list_item')]")).shouldHave(CollectionCondition.size(4));



    }
}
