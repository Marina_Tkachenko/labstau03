import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.firefox.FirefoxDriver;
        import org.testng.annotations.Test;

public class TestBaseFireFox {

    @Test
    public void loginWebex(){
        System.setProperty("webdriver.gecko.driver", "C:\\Gitea\\WMSTI_Test_Automation\\src\\main\\resources\\Geckodriver\\geckodriver.exe");
        WebDriver driverFireFox = new FirefoxDriver();
        driverFireFox.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        driverFireFox.quit();
    }
}
