import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver;

    @BeforeClass
    public void init() {
        System.setProperty("webdriver.chrome.driver", "C:\\Gitea\\WMSTI_Test_Automation\\src\\main\\resources\\ChromeDriver\\chromedriver92.exe");
        driver = new ChromeDriver();
        driver.get("https://dtag.webex.com/webappng/sites/dtag/dashboard?siteurl=dtag");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }
    @BeforeClass
    public void signInWebex(){
        WebElement signInButton = driver.findElement(By.xpath("//button[@id='guest_signin_split_button-action']"));
        Assert.assertTrue(signInButton.isDisplayed(), "Sign InButton is displayed");
        signInButton.click();

        WebElement emailAddressField = driver.findElement(By.id("IDToken1"));
        emailAddressField.sendKeys("Marina.Tkachenko@telekom.com");

        WebElement buttonSignInAfterFillingEmailField = driver.findElement(By.name("btnOK"));
        buttonSignInAfterFillingEmailField.click();

        WebElement messageToCheck = driver.findElement(By.id("openingMessage"));
        Assert.assertTrue(messageToCheck.isDisplayed(),"messageToCheck is displayed");

        driver.findElement(By.xpath("//*[@id=\"bySelection\"]/div[3]/div")).click();
        Assert.assertEquals(driver.getCurrentUrl(),"https://myportal-websso.corp.telekom.de/login/?target=https://myportal-websso.corp.telekom.de:443/sps/auth","Url is correct");





    }

    @AfterClass
    public void tearDown(){
//        driver.quit();
    }
}
